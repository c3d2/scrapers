#!/usr/bin/env ruby

require 'open-uri'
require 'json'
require 'influxdb'

HOST = ARGV[0]
USER = ARGV[1]
PASSWORD = ARGV[2]

points = []

data = JSON.parse URI::open("https://#{HOST}/statistics.json", :http_basic_authentication => [USER, PASSWORD]).read()
points.push({
  series: 'statistics',
  values: data.map { |(n, i)| [n, i.to_i] },
})

data = JSON.parse URI::open("https://#{HOST}/summary.json", :http_basic_authentication => [USER, PASSWORD]).read()
data.each do |d|
  points.push({
    series: 'summary',
    tags: {
      name: d["name"],
    },
    values: {
      price: d["price"].to_f,
      value: d["value"],
      volume: d["volume"].to_f,
    }
  })
end

db = InfluxDB::Client.new(url: "http://grafana.serv.zentralwerk.org:8086/matemat")
db.write_points(points, 'm')
