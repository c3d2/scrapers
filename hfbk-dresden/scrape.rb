#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'nokogiri'
require 'erb'
require 'uri'

events = []

url = "https://www.hfbk-dresden.de/veranstaltungen"
doc = Nokogiri::HTML URI.open(url)
doc.css("h2 a").each do |a|
  link = "https://www.hfbk-dresden.de" + a.attr("href")
  page = Nokogiri::HTML URI.open(link)
  page.css("a").each do |a|
    href = URI.decode_www_form_component a.attr("href")
    if href =~ /^data:text\/calendar.*BEGIN:VEVENT(.+)END:VEVENT$/m
      ev = "BEGIN:VEVENT\n#{$1.strip}\nURL:#{link}\nEND:VEVENT\n"
      events << ev
    end
  end
end

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin
  <% events.each do |ev| %>
  <%= ev %>
  <% end %>
  END:VCALENDAR
EOF

puts ical.result
