#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'nokogiri'
require 'uri'
require 'json'
require 'erb'

JSON.parser = JSON::Ext::Parser


def fmt_time t
  t.strftime "%Y%m%dT%H%M%S"
end

class Event
  attr_accessor :title, :url, :locations, :dtstart, :dtend

  def initialize
    @locations = []
  end
end

events = []

url = "https://www.bibo-dresden.de/de/veranstaltungen/veranstaltungskalender.php"
STDERR.puts "GET #{url}"
doc = Nokogiri::HTML URI.open(url)
doc.css(".event_link").each do |a|
  event_url = URI.join url, a.attr('href')
  STDERR.puts "GET #{event_url}"
  event_doc = Nokogiri::HTML URI.open(event_url)
  event_doc.css("script[type='application/ld+json']").each do |script|
    begin
      content = script.text
      json = JSON::parse content
      next unless json['@context'] == "http://schema.org" && json['@type'] == 'Event'

      ev = Event::new
      ev.title = json['name']
      ev.url = json['url']
      ev.dtstart = Time.parse(json['startDate'])
      ev.dtend = Time.parse(json['endDate'])
      ev.locations << json["location"]["name"]
      ev.locations << json["location"]["address"]["streetAddress"]
      events << ev
    rescue
      STDERR.puts "Omitting: #{$!}"
      STDERR.puts $!.backtrace
    end
  end
end

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin
  <% events.each do |ev| %>
  BEGIN:VEVENT
  SUMMARY:<%= ev.title %>
  DTSTART:<%= fmt_time ev.dtstart %>
  DTEND:<%= fmt_time ev.dtend %>
  UID:<%= ev.url %>
  URL:<%= ev.url %>
  LOCATION:<%= ev.locations.join ", " %>
  END:VEVENT
  <% end %>
  END:VCALENDAR
EOF

puts ical.result
