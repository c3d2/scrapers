#!/usr/bin/env ruby

require 'open-uri'
require 'json'
require 'influxdb'

SENSOR_LOCATIONS = {
  44132 => "Wurzelwerk Tunnel",
  44133 => "Wurzelwerk Tunnel",
}
TIME_PRECISION = 's'

data = JSON.parse open("https://data.sensor.community/static/v2/data.json").read()

points = []
data.each do |d|
  id = d["sensor"]["id"]

  if SENSOR_LOCATIONS.has_key? id
    values = {}
    d["sensordatavalues"].each do |sensordatavalue|
      values[sensordatavalue["value_type"]] = sensordatavalue["value"].to_f
    end
    sensor_type = d["sensor"]["sensor_type"]["name"]
    if sensor_type == "SDS011"
      values = {
        "SDS_P1" => values["P1"],
        "SDS_P2" => values["P2"],
      }
    end
    points.push({
      series: "luftdaten",
      tags: {
        node: SENSOR_LOCATIONS[id],
        sensor_type: sensor_type,
      },
      values: values,
      timestamp: InfluxDB.convert_timestamp(Time.parse(d["timestamp"] + " UTC"), TIME_PRECISION),
    })
  end
end
pp points

db = InfluxDB::Client.new(url: "http://grafana.serv.zentralwerk.org:8086/luftdaten")
db.write_points points, TIME_PRECISION
