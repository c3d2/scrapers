#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'nokogiri'
require 'erb'

def fmt_time t
  t.strftime "%Y%m%dT%H%M%S"
end

class Event
  attr_accessor :title, :url, :location, :image
  attr_reader :dtstart, :dtend

  def date=(s)
    if s =~ /(\d+)\.(\d+)\.(\d+)\s[\-—]\s(\d+)\.(\d+)\.(\d+)/
      year1 = $3.to_i
      month1 = $2.to_i
      day1 = $1.to_i
      year2 = $6.to_i
      month2 = $5.to_i
      day2 = $4.to_i
      @dtstart = Time::local(year1, month1, day1, 0, 0, 0)
      @dtend = Time::local(year2, month2, day2, 0, 0, 0) + 86400
    elsif s =~ /(\d+)\.(\d+)\.(\d+)/
      year1 = $3.to_i
      month1 = $2.to_i
      day1 = $1.to_i
      @dtstart = Time::local year1, month1, day1, 0, 0, 0
      @dtend = @dtstart + 86400
    else
      raise "Invalid date: #{s.inspect}"
    end
  end
end

events = []

url = "https://dresdencontemporaryart.com/de"
doc = Nokogiri::HTML URI.open(url)
doc.css("#masonry div").each do |block|
  links = block.css("a.info.block")
  next if links.length != 3

  begin
    ev = Event::new
    ev.title = links[0].css("h3").text
    ev.url = URI.join(url, links[0].attr('href')).to_s
    ev.date = links[0].css("p").text
    ev.location = links[1].css("p").text
    if (img = links[2].css("img")) and (src = img.attr('src'))
      path = src && src.text.split("/")
      # Fix raw umlauts
      path[path.length - 1] = URI.encode_uri_component path[path.length - 1]
      src = path.join("/")
      ev.image = URI.join(url, src).to_s
    end
    events << ev
  rescue
    STDERR.puts "Omitting: #{$!}"
    STDERR.puts $!.backtrace
  end
end

template = <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin
  <% events.each do |ev| %>
  BEGIN:VEVENT
  SUMMARY:<%= ev.title %>
  DTSTART:<%= fmt_time ev.dtstart %>
  DTEND:<%= fmt_time ev.dtend %>
  UID:<%= ev.url %>
  URL:<%= ev.url %>
  LOCATION:<%= ev.location %>
  <% if ev.image -%>
  ATTACH;FMTTYPE=image/jpeg:<%= ev.image %>
  <% end -%>
  END:VEVENT
  <% end %>
  END:VCALENDAR
EOF

puts ERB::new(template, trim_mode: '-').result
