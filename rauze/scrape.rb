#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'json'
require 'nokogiri'
require 'erb'
require 'date'

events = []

(-1..21).each do |day|
  url = "https://www.rauze.de/?date=#{Date.today + day}"
  STDERR.puts "GET #{url}"

  doc = Nokogiri::HTML URI.open(url).read
  doc.css(".cal-dav a").each do |a|
    link = URI.join url, a.attr('href')
    event_line = false
    has_url = false
    STDERR.puts "GET #{link}"
    ical = URI.open(link).read
    events.push ical.lines.collect { |line|
      case line.chomp
      when "BEGIN:VEVENT"
        event_line = true
        line
      when "END:VEVENT"
        event_line = false
        if has_url
          line
        else
          url = link.to_s.sub(/\?.*/, "")
          ["URL:#{url}\n", line]
        end
      when /URL:/
        if event_line
          has_url = true
          line
        else
          []
        end
      else
        event_line ? line : []
      end
    }.flatten.join
  end
end

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin

  <%= events.join "\n" %>

  END:VCALENDAR
EOF

puts ical.result
