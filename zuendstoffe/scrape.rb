#!/usr/bin/env ruby
# coding: utf-8

require 'uri'
require 'net/http'
require 'json'
require 'time'
require 'erb'

url = "https://zuendstoffe.materialvermittlung.org/graphql"
uri = URI.parse(url)
http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = uri.is_a?(URI::HTTPS)

req = Net::HTTP::Post.new(uri.request_uri, 
  'Content-Type' => 'application/json'
)
req.body = {
  "operationName" => "MaterialQuery",
  "variables" => {
    "hasAvailableQuantity" => true,
    "first" => 9000,
  },
  "query" => <<-EOF
    query MaterialsQuery($first: Int, $hasAvailableQuantity: Boolean) {
      materials(first: $first hasAvailableQuantity: $hasAvailableQuantity) {
        edges {
          node {
            id
            _id
            title
            description
            isNew
            isDraft
            isFinished
            quantityUnit
            availableQuantity
            inflowQuantity
            pickedUpQuantity
            reservedQuantity
            publishAt
            updatedAt
            visibleUntil
            validationResults
            color
            dimensions
            isDraft
            visibleUntil
            disallowPartialReservations
            images {
              id
              thumbnailUrl
              previewUrl
              detailsUrl
              __typename
            }
            storage {
              id
              _id
              title
              isPublic
              notes
              addressStreet
              addressPostalCode
              addressCity
              contact
              __typename
            }
            __typename
          }
        }
      }
    }
  EOF
}.to_json

res = http.request(req)
if res.code != "200"
  raise "HTTP #{res.code}"
end


class Material
  attr_accessor :link
  attr_accessor :title
  attr_accessor :descriptions
  attr_accessor :updated
  attr_accessor :published
  attr_accessor :images
  attr_accessor :storages
end

json = JSON.parse(res.body)
materials = json["data"]["materials"]["edges"].collect do |edge|
  edge["node"]
end.collect do |node|
  begin
    m = Material.new
    m.link = URI.join(uri, "/material/#{node["_id"]}")
    m.title = node["title"]
    if node["availableQuantity"] and node["quantityUnit"]
      m.title = "#{node["availableQuantity"]} #{node["quantityUnit"]} #{m.title}"
    end
    m.descriptions = [node["color"], node["dimensions"], node["description"]]
      .filter { |s| s.to_s.size > 1 }
    m.published = Time.parse(node["publishAt"])
    m.updated = Time.parse(node["publishAt"])
    m.images = node["images"].collect do |img|
      img["detailsUrl"] ? URI.join(uri, img["detailsUrl"]) : nil
    end.compact
    s = node["storage"]
    m.storages = [
      s["notes"],
      s["contact"],
      s["title"],
      s["addressStreet"],
      "#{s["addressPostalCode"]} #{s["addressCity"]}",
    ].collect { |s| s.to_s.strip }
      .filter { |s| s.to_s.size > 1 }
    m
  rescue
    STDERR.puts $!.inspect
    nil
  end
end.compact

def escape_xml s
  s.to_s
    .gsub(/&/, "&amp;")
    .gsub(/</, "&lt;")
    .gsub(/>/, "&gt;")
    .gsub(/"/, "&quot;")
end

atom = ERB::new <<~EOF
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title>Zuendstoffe Materialvermittlung</title>
  <link href="https://zuendstoffe.materialvermittlung.org/"/>
  <updated><%= Time.now.iso8601 %></updated>

  <% materials.each do |m| %>
    <entry>
      <title><%= escape_xml m.title %></title>
      <published><%= m.published.iso8601 %></published>
      <updated><%= m.updated.iso8601 %></updated>
      <link rel="alternate" type="text/html" href="<%= escape_xml m.link %>"/>
      <id><%= escape_xml m.link %></id>

      <content type="xhtml">
        <div xmlns="http://www.w3.org/1999/xhtml">
          <% m.descriptions.each do |d| %>
            <p><%= escape_xml d %></p>
          <% end %>
          <% m.images.each do |i| %>
            <img src="<%= escape_xml i %>"/>
          <% end %>
          <% m.storages.each do |s| %>
            <p><%= escape_xml s %></p>
          <% end %>
        </div>
      </content>
    </entry>
  <% end %>
</feed>
EOF
puts atom.result
