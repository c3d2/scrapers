#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'nokogiri'
require 'erb'
require 'uri'

events = []
seen_urls = {}

url = "https://zwickmuehle.org/termine/"
STDERR.puts "GET HTML #{url}"
doc = Nokogiri::HTML URI.open(url)
doc.css("h2 a").each do |a|
  link = URI.join(url, a.attr("href")).to_s
  next if seen_urls.has_key? link
  seen_urls[link] = true

  STDERR.puts "GET HTML #{link}"
  event_doc = Nokogiri::HTML URI.open(link)
  event_doc.css("a[text() = '+ iCAL']").each do |a|
    ics_link = URI.join(link, a.attr("href")).to_s
    next if seen_urls.has_key? ics_link
    seen_urls[ics_link] = true

    STDERR.puts "GET ICS #{ics_link}"
    ics = URI.open(ics_link).read
    if ics =~ /BEGIN:VEVENT(.+)END:VEVENT/m
      ev = "BEGIN:VEVENT\n#{$1.strip}\nURL:#{link}\nEND:VEVENT\n"
      events << ev
    end
  end
end

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin
  <% events.each do |ev| %>
  <%= ev %>
  <% end %>
  END:VCALENDAR
EOF

puts ical.result
