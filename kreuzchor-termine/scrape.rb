#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'json'
require 'erb'

def fmt_time t
  t.strftime "%Y%m%dT%H%M%S"
end

class Event
  attr_accessor :name, :location, :link, :date
end

events = []

url = "https://kreuzchor.de/termine-tickets/"
data = URI.open(url).read()
if data =~ /var php_vars = (\{.+\});$/
  json = JSON.parse $1
else
  raise "Data not found"
end

json['events'].each do |event|
  ev = Event::new
  ev.name = event['title']
  ev.location = event['venue']
  ev.date = Time.at(event['event_time'] / 1000, in: 0)
  ev.link = event['permalink'].empty? ? event['event_link'] : event['permalink']
  events << ev
end

events.sort_by! { |ev| ev.date }

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin
  <% events.each do |ev| %>
  BEGIN:VEVENT
  METHOD:PUBLISH
  CLASS:PUBLIC
  UID:<%= ev.link %>
  DTSTART:<%= fmt_time(ev.date) %>
  DTEND:<%= fmt_time(ev.date + 2 * 3600) %>
  SUMMARY:<%= ev.name %>
  LOCATION:<%= ev.location %>
  URL:<%= ev.link %>
  END:VEVENT
  <% end %>
  END:VCALENDAR
EOF

puts ical.result
