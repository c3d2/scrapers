<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    exclude-result-prefixes="xsl xsi">

  <xsl:output method="xml"
	    version="1.0"
            encoding="utf-8"
            indent="yes"/>

  <xsl:template match="/schedule">
    <schedule>
      <xsl:apply-templates/>
    </schedule>
  </xsl:template>
  <xsl:template match="/schedule/day">
    <day index="{@day}" date="{@date}" start="{@start}" end="{@end}">
      <xsl:apply-templates/>
    </day>
  </xsl:template>
  <xsl:template match="/schedule/day/room">
    <room name="{@name}" guid="{@guid}">
      <xsl:apply-templates/>
    </room>
  </xsl:template>
  <xsl:template match="/schedule/day/room/event">
    <event guid="{@guid}" id="{@id}">
      <xsl:apply-templates/>
    </event>
  </xsl:template>
  <xsl:template match="/schedule/day/room/event/duration">
    <duration>
      <xsl:variable name="minutes" select="substring-after(., ':')"/>
      <xsl:choose>
        <xsl:when test="contains($minutes, ':')">
          <xsl:text>00:30</xsl:text>
        </xsl:when>
        <xsl:when test="number($minutes) &gt;= 60">
          <xsl:value-of select="substring-before(., ':')"/>
          <xsl:text>:</xsl:text>
          <xsl:text>00</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="substring-before(., ':')"/>
          <xsl:text>:</xsl:text>
          <xsl:value-of select="$minutes"/>
        </xsl:otherwise>
      </xsl:choose>
    </duration>
  </xsl:template>

  <xsl:template match="*">
    <xsl:copy-of select="."/>
  </xsl:template>
</xsl:stylesheet>
