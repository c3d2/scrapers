#! /usr/bin/env bash

set -e

curl -s https://events.ccc.de/camp/2023/hub/api/c/camp23/schedule.xml | xsltproc fix.xsl -
