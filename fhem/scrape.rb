#!/usr/bin/env ruby

require 'open-uri'
require 'nokogiri'
require 'influxdb'

HOST = ARGV[0]
USER = ARGV[1]
PASSWORD = ARGV[2]

points = []

doc = Nokogiri::HTML URI::open("https://#{HOST}/fhem?room=all", :http_basic_authentication => [USER, PASSWORD])
doc.css('.roomoverview tr').each do |row|
  name = row.css('.col1').text
  current = row.css('.col2').text
  desired = row.css('.col3 .fhemWidget').attr('current')
  desired = desired.value if desired
  if !name.empty? and name.length < 40
    values = {}
    if current =~ /^\d/
      values[:temperature] = current.to_f
    end
    if desired =~ /^\d/
      values[:desiredTemperature] = desired.to_f
    end
    values[:onoff] = if desired == "off" then 1.0 else 0.0 end

    points.push({
      series: 'iot',
      tags: {
        instance: name,
      },
      values: values,
    })
  end
end

db = InfluxDB::Client.new(url: "http://grafana.serv.zentralwerk.org:8086/iot")
db.write_points points, 'm'
