#!/usr/bin/env ruby

require 'open-uri'
require 'json'
require 'erb'

def scrape(year)
  JSON.parse URI.open("https://criticalmass.in/api/ride?citySlug=dresden&year=#{year}")
               .read()
end

def fmt_time(stamp)
  Time.at(stamp)
    .strftime "%Y%m%dT%H%M%S"
end

events = (scrape(Time.now.year) + scrape(Time.now.year + 1))

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin
  <% events.each do |ev| %>
  BEGIN:VEVENT
  SUMMARY:<%= ev['title'] %>
  DTSTART:<%= fmt_time ev['date_time'] %>
  DTEND:<%= fmt_time ev['date_time'] + 3600 * (ev['estimated_duration'] or 2) %>
  UID:<%= ev['id'] %>@ciritcalmass.in
  URL:https://criticalmass.in/dresden
  LOCATION:<%= ev['location'] %>
  GEO:<%= ev['latitude'] %>;<%= ev['longitude'] %>
  END:VEVENT
  <% end %>
  END:VCALENDAR
EOF
puts ical.result
