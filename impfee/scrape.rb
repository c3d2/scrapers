#!/usr/bin/env ruby

require 'open-uri'
require 'json'
require 'influxdb'

# https://www.countee.ch/app/de/counter/impfee/_iz_sachsen
data = JSON.parse URI::open("https://www.startupuniverse.ch/api/1.1/de/counters/getAll/_iz_sachsen").read()

points = []
data['response']['data'].each do |id, iz|
  next if iz['closed'] != 0
  sum = iz['counteritems'].map { |item| item['val'] }.sum

  points.push({
                series: "iz",
                tags: {
                  name: iz['name'],
                  slug: iz['slug'],
                },
                values: {
                  sum: sum,
                },
              })
end

db = InfluxDB::Client.new(url: "http://grafana.serv.zentralwerk.org:8086/impfee")
db.write_points points
