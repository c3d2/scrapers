#!/usr/bin/env ruby
# coding: utf-8

require 'open-uri'
require 'nokogiri'
require 'erb'

MONTHS = [
  "Januar", "Februar", "März", "April",
  "Mai", "Juni", "Juli", "August",
  "September", "Oktober", "November", "Dezember",
]

def fmt_time t
  t.strftime "%Y%m%dT%H%M%S"
end

class Event
  attr_accessor :title, :url
  attr_reader :date, :location, :dtstart, :dtend

  def initialize
    @location = "kosmotique, Martin-Luther-Str. 13, 01099 Dresden"
  end

  def date=(s)
    if s =~ /(\d{1,2})\.\s*(.+?)\s*(\d{4})[^\d]*(\d{1,2}):(\d{2})[^\d]*(\d{1,2}):(\d{2})/
      year = $3.to_i
      month = MONTHS.index($2) + 1
      day = $1.to_i
      @dtstart = Time::local year, month, day, $4.to_i, $5.to_i, 0
      @dtend = Time::local year, month, day, $6.to_i, $7.to_i, 0
    elsif s =~ /(\d{1,2})\.\s*(.+?)\s*(\d{4})[^\d]*(\d{1,2}):(\d{2})/
      year = $3.to_i
      month = MONTHS.index($2) + 1
      day = $1.to_i
      @dtstart = Time::local year, month, day, $4.to_i, $5.to_i, 0
      @dtend = Time::local year, month, day, $4.to_i + 2, $5.to_i, 0
    else
      raise "Invalid date: #{s.inspect}"
    end

    while @dtend < @dtstart
      # @dtend is in new day
      @dtend += 86400
    end
  end
end

events = []

url = "https://www.kosmotique.org/"
doc = Nokogiri::HTML URI.open(url)
doc.css("article").each do |termin|
  begin
    ev = Event::new
    ev.title = termin.css("h2").text
    ev.url = URI.join url, termin.css("a")[0].attr("href")
    ev.date = termin.css(".datum").text
    events << ev
  rescue
    STDERR.puts "Omitting: #{$!}"
    # STDERR.puts $!.backtrace
  end
end

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin
  <% events.each do |ev| %>
  BEGIN:VEVENT
  SUMMARY:<%= ev.title %>
  DTSTART:<%= fmt_time ev.dtstart %>
  DTEND:<%= fmt_time ev.dtend %>
  UID:<%= ev.url %>
  URL:<%= ev.url %>
  LOCATION:<%= ev.location %>
  END:VEVENT
  <% end %>
  END:VCALENDAR
EOF

puts ical.result
