#!/usr/bin/env ruby
# coding: utf-8

require 'uri'
require 'open-uri'
require 'nokogiri'
require 'erb'

def fmt_time t
  Time.parse(t).strftime "%Y%m%dT%H%M%S"
end

class Event
  attr_accessor :title, :url, :location, :dtstart, :dtend
end

events = []

seen_urls = {}
url = URI::parse "https://dresden.de/apps_ext/IKT/"
while url
  STDERR.puts "GET #{url}"
  seen_urls[url.to_s] = true
  doc = Nokogiri::HTML URI.open(url)
  doc.css(".event").each do |content|
    ev = Event::new
    ev.title = content.css("h1.h3").text.strip.gsub(/[\r\n]/, "")
    ev.url = URI.join(url, content.css("a.event-link").attr('href'))
               .to_s
               .sub(/;jsessionid.*/, "")
    ev.location = content.css(".location .segment-content div").collect { |div|
      div.text
    }.join(", ").gsub(/[\r\n]/, "")
    ev.dtstart = fmt_time content.css(".datetime .start").attr("datetime")
    ev.dtend = fmt_time content.css(".datetime .start").attr("datetime")

    events << ev
  end

  next_url = nil
  doc.css(".next").each do |a|
    href = a.attr("href")
    next_url = URI.join url, href if href
  end
  break if seen_urls[next_url.to_s]
  url = next_url
end

ical = ERB::new <<~EOF
  BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  X-WR-TIMEZONE;VALUE=TEXT:Europe/Berlin
  <% events.each do |ev| %>
  BEGIN:VEVENT
  SUMMARY:<%= ev.title %>
  DTSTART:<%= fmt_time ev.dtstart %>
  DTEND:<%= fmt_time ev.dtend %>
  UID:<%= ev.url %>
  URL:<%= ev.url %>
  LOCATION:<%= ev.location %>
  END:VEVENT
  <% end %>
  END:VCALENDAR
EOF

puts ical.result
